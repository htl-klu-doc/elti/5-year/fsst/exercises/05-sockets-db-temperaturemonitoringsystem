package at.htlklu.fsst.helper;

import java.util.Set;
import java.util.stream.Collectors;

import at.htlklu.fsst.dao.RoomDao;
import at.htlklu.fsst.pojo.Room;
import at.htlklu.fsst.pojo.Temperature;

public class Helper {
	public static String getLastTemp(String roomNumber) {
		Temperature temp = RoomDao.getRoomTemperature(new Room(roomNumber));

		if (temp == null)
			return "Keine Daten vorhanden";
		else
			return temp.getTemperatureBasic();
	}

	public static String printAll() {
		return RoomDao.getAllRooms().stream()
				.map(room -> String.format("(%s: %s)", room, RoomDao.getAllRoomTemperatures(room)))
				.collect(Collectors.toList()).toString();
	}

	public static String set(String data) {
		String[] tokens = data.split(":");

		if (tokens == null || tokens.length != 2)
			return "Invalid format! Use SET <room identifier>:<temperature>";

		String roomIdentifier = tokens[0];
		double temperatureValue = 0;
		try {
			temperatureValue = Double.parseDouble(tokens[1]);
		} catch (NumberFormatException e) {
			return String.format("»%s« has the wrong format!", tokens[1]);
		}
		Room room = new Room(roomIdentifier);
		Temperature temperature = new Temperature(temperatureValue, room);
		RoomDao.setRoomTemperature(room, temperature);
		return "Success :)";
	}

	public static String listRooms(Set<Room> rooms) {
		return rooms.stream().collect(Collectors.toList()).toString();
	}

	public static String listAllRooms() {
		return listRooms(RoomDao.getAllRooms());
	}
}
