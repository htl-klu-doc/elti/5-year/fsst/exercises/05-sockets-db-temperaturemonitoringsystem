package at.htlklu.fsst.pojo;

import java.time.Instant;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

public class Temperature {
	private int id;
	private double value;
	private Instant instant;
	private String roomName;

	public Temperature(double temperature, Room room) {
		this.setTemperature(temperature);
		this.setRoomName(room.getName());
	}

	public Temperature(int id, double value, Instant instant, String roomName) {
		this.setId(id);
		this.setValue(value);
		this.setInstant(instant);
		this.setRoomName(roomName);
	}

	@Override
	public String toString() {
		return String.format("%s°C - %s", this.getTemperatureBasic(), this.getInstant().toString());
	}

	public static String printAll(Map<Room, Temperature> rooms) {
		return rooms.entrySet().stream().map(temp -> temp.getValue().toString()).collect(Collectors.toList())
				.toString();
	}

	public double getTemperature() {
		return this.value;
	}

	public String getTemperatureBasic() {
		return String.format(Locale.ROOT, "%.1f", this.value);
	}

	public void setTemperature(double temperature) {
		this.value = temperature;
	}

	public Instant getInstant() {
		return instant;
	}

	public void setInstant(Instant instant) {
		this.instant = instant;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}
	
}
