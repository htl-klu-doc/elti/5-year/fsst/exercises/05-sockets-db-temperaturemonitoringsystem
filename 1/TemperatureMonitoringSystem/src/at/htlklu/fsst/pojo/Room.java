package at.htlklu.fsst.pojo;

public class Room {
	private String name;

	public Room(String name) {
		this.setName(name);
	}

	@Override
	public String toString() {
		return String.format("%s", this.getName());
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
