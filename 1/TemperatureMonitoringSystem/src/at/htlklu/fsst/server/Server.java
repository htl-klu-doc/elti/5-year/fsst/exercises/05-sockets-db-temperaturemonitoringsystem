package at.htlklu.fsst.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import at.htlklu.fsst.helper.Helper;

public class Server extends Thread {
	public static final int PORT = 5710;

	private Socket socket;
	private PrintWriter out;
	private BufferedReader in;
	private boolean keepRunning = true;

	public static void main(String[] args) {
		try (ServerSocket serverSocket = new ServerSocket(PORT);) {
			while (true) {
				Server server = new Server(serverSocket.accept());
				server.start();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Server(Socket s) {
		this.socket = s;

		try {
			this.out = new PrintWriter(socket.getOutputStream(), true);
			this.in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		out.println("Welcome :)");
		while (keepRunning) {
			try {
				out.println(parseLine(in.readLine()));
			} catch (IOException e) {
				e.printStackTrace();
				keepRunning = false;
			}
		}
		try {
			this.socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String checkInput(String readLine, String command) {
		int commandLength = command.length();
		if (commandLength + 1 > readLine.length())
			return null;

		return readLine.substring(commandLength + 1);
	}

	private String parseLine(String readLine) {
		if (readLine == null)
			return "Invalid input";

		String[] tokens = readLine.split(" ");

		if (tokens[0] == null)
			return "Invalid input";

		switch (tokens[0]) {
		case "SET" -> {
			String data = checkInput(readLine, "SET");
			if (data == null)
				return "Missing values!";
			return Helper.set(data);
		}
		case "GET" -> {
			String data = checkInput(readLine, "GET");
			if (data == null)
				return "Missing room identifier";
			return Helper.getLastTemp(data);
		}
		case "LIST" -> {
			return Helper.listAllRooms();
		}
		case "ALL" -> {
			return Helper.printAll();
		}
		case "EXIT" -> {
			this.keepRunning = false;
			return "Bye";
		}
		default -> {
			return "Error: Unknown command.";
		}
		}
	}

}
