package at.htlklu.fsst.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import at.htlklu.fsst.helper.DbConnection;
import at.htlklu.fsst.pojo.Room;
import at.htlklu.fsst.pojo.Temperature;

public class TemperatureDao {
	protected static int insertTemperature(Temperature temperature, Room room) {
		try (PreparedStatement pstmt = DbConnection.getConnection()
				.prepareStatement("INSERT INTO temperature (value, room_name) VALUES (?, ?)")) {
			pstmt.setDouble(1, temperature.getValue());
			pstmt.setString(2, room.getName());
			return pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}

	protected static Temperature getLastTemperature(Room room) {
		try (PreparedStatement pstmt = DbConnection.getConnection()
				.prepareStatement("SELECT * FROM temperature WHERE room_name = ? ORDER BY instant DESC LIMIT 1")) {
			pstmt.setString(1, room.getName());
			ResultSet rset = pstmt.executeQuery();
			if (rset.next())
				return mapRowTemperature(rset);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	protected static List<Temperature> getAllTemperatures(Room room) {
		ArrayList<Temperature> temperatures = new ArrayList<>();
		try (PreparedStatement pstmt = DbConnection.getConnection()
				.prepareStatement("SELECT * FROM temperature WHERE room_name = ? ORDER BY instant DESC")) {
			pstmt.setString(1, room.getName());
			ResultSet rset = pstmt.executeQuery();
			while (rset.next())
				temperatures.add(mapRowTemperature(rset));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return temperatures;
	}

	private static Temperature mapRowTemperature(ResultSet rset) throws SQLException {
		return new Temperature(rset.getInt(1), rset.getDouble(2), rset.getTimestamp(3).toInstant(), rset.getString(4));
	}

}
