package at.htlklu.fsst.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import at.htlklu.fsst.helper.DbConnection;
import at.htlklu.fsst.pojo.Room;
import at.htlklu.fsst.pojo.Temperature;

public class RoomDao {
	public static Set<Room> getAllRooms() {
		HashSet<Room> rooms = new HashSet<>();

		try (PreparedStatement pstmt = DbConnection.getConnection().prepareStatement("SELECT * FROM room")) {
			ResultSet rset = pstmt.executeQuery();
			while (rset.next())
				rooms.add(mapRowRoom(rset));
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return rooms;
	}

	public static int setRoomTemperature(Room room, Temperature temperature) {
		if (!roomAlreadyExists(room))
			insertRoom(room);

		return TemperatureDao.insertTemperature(temperature, room);
	}

	public static Temperature getRoomTemperature(Room room) {
		if (!roomAlreadyExists(room))
			return null;

		return TemperatureDao.getLastTemperature(room);
	}

	public static List<Temperature> getAllRoomTemperatures(Room room) {
		if (!roomAlreadyExists(room))
			return null;

		return TemperatureDao.getAllTemperatures(room);
	}

	private static int insertRoom(Room room) {
		try (PreparedStatement pstmt = DbConnection.getConnection()
				.prepareStatement("INSERT INTO room (name) VALUES (?)")) {
			pstmt.setString(1, room.getName());
			return pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}

	private static boolean roomAlreadyExists(Room room) {
		HashSet<Room> rooms = new HashSet<>();
		try (PreparedStatement pstmt = DbConnection.getConnection()
				.prepareStatement("SELECT * FROM room WHERE name = ?")) {
			pstmt.setString(1, room.getName());
			ResultSet rset = pstmt.executeQuery();
			while (rset.next())
				rooms.add(mapRowRoom(rset));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rooms.size() == 1;
	}

	private static Room mapRowRoom(ResultSet rset) throws SQLException {
		return new Room(rset.getString(1));
	}
}
