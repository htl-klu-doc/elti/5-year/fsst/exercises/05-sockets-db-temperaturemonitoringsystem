package at.htlklu.fsst;
// TODO

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.awt.event.ActionEvent;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextArea;
import java.awt.Dimension;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Component;

public class Client extends JFrame {
	private static final long serialVersionUID = 2297449611745457213L;
	public static final int PORT = 5710;
	private Socket s;
	private BufferedReader in;
	private PrintWriter out;

	private JPanel contentPane;
	private JTextArea tarOut;
	private JTextField txtRoom;
	private JTextField txtTemp;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Client frame = new Client();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Client() {
		setMinimumSize(new Dimension(203, 297));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 425, 297);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		JButton btnConnect = new JButton("Connect");
		btnConnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					if (s == null) {
						s = new Socket("localhost", PORT);
						in = new BufferedReader(new InputStreamReader(s.getInputStream()));
						out = new PrintWriter(s.getOutputStream(), true);
						tarOut.setText(in.readLine());
						btnConnect.setText("Disconnect");
					} else {
						out.println("EXIT");
						s.close();
						s = null;
						btnConnect.setText("Connect");
					}
				} catch (UnknownHostException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});

		JButton btnSet = new JButton("Set");
		btnSet.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (s != null && s.isConnected()) {
					String room = txtRoom.getText();
					String temp = txtTemp.getText();
					out.println(String.format("SET %s:%s", room, temp));

					try {
						tarOut.setText(in.readLine());
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			}
		});

		JButton btnGet = new JButton("Get");
		btnGet.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (s != null && s.isConnected()) {
					String room = txtRoom.getText();
					out.println(String.format("GET %s", room));
					try {
						tarOut.setText(in.readLine());
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			}
		});

		tarOut = new JTextArea();
		tarOut.setLineWrap(true);
		tarOut.setEditable(false);

		txtRoom = new JTextField();
		txtRoom.setText("T015");
		txtRoom.setColumns(10);

		JLabel lblRoom = new JLabel("Room");

		txtTemp = new JTextField();
		txtTemp.setText("29.5");
		txtTemp.setColumns(10);

		JLabel lblTemperature = new JLabel("Temperature");

		JButton btnList = new JButton("List");
		btnList.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (s != null && s.isConnected()) {
					out.println("LIST");
					try {
						tarOut.setText(in.readLine());
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			}
		});

		JButton btnAll = new JButton("All");
		btnAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (s != null && s.isConnected()) {
					out.println("ALL");
					try {
						tarOut.setText(in.readLine());
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			}
		});
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING).addGroup(gl_contentPane
				.createSequentialGroup().addContainerGap()
				.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING).addGroup(gl_contentPane
						.createSequentialGroup().addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING, false)
								.addComponent(btnConnect, Alignment.LEADING, GroupLayout.DEFAULT_SIZE,
										GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addGroup(Alignment.LEADING,
										gl_contentPane.createSequentialGroup()
												.addComponent(btnSet, GroupLayout.PREFERRED_SIZE, 89,
														GroupLayout.PREFERRED_SIZE)
												.addPreferredGap(ComponentPlacement.RELATED)
												.addComponent(btnGet, GroupLayout.PREFERRED_SIZE, 92,
														GroupLayout.PREFERRED_SIZE)
												.addPreferredGap(ComponentPlacement.RELATED)
												.addComponent(btnList, GroupLayout.PREFERRED_SIZE, 92,
														GroupLayout.PREFERRED_SIZE)
												.addPreferredGap(ComponentPlacement.RELATED)
												.addComponent(btnAll, GroupLayout.PREFERRED_SIZE, 91,
														GroupLayout.PREFERRED_SIZE)))
						.addGap(4))
						.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup().addGroup(gl_contentPane
								.createParallelGroup(Alignment.TRAILING)
								.addComponent(tarOut, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 385, Short.MAX_VALUE)
								.addGroup(gl_contentPane.createSequentialGroup()
										.addComponent(lblTemperature, GroupLayout.PREFERRED_SIZE, 84,
												GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.RELATED, 197, Short.MAX_VALUE)
										.addComponent(txtTemp, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_contentPane.createSequentialGroup()
										.addComponent(lblRoom, GroupLayout.PREFERRED_SIZE, 56,
												GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.RELATED, 225, Short.MAX_VALUE)
										.addComponent(txtRoom, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE)))
								.addGap(6)))));
		gl_contentPane.setVerticalGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup().addContainerGap()
						.addComponent(tarOut, GroupLayout.PREFERRED_SIZE, 108, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(txtRoom, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(lblRoom))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(txtTemp, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(lblTemperature))
						.addPreferredGap(ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE).addComponent(btnGet)
								.addComponent(btnSet).addComponent(btnList).addComponent(btnAll))
						.addPreferredGap(ComponentPlacement.RELATED).addComponent(btnConnect).addContainerGap()));
		gl_contentPane.linkSize(SwingConstants.HORIZONTAL, new Component[] { btnSet, btnGet, btnList, btnAll });
		contentPane.setLayout(gl_contentPane);
	}
}
